
import java.util.*;
public class tic_tac_toe {
	static Scanner scan=new Scanner(System.in);
	static void play(){
			int number=-1,pemain=1,pilih;
			char[][]tictactoe=new char[3][3];
			for(int baris=0 ;baris<3; baris++){
				for(int kolom=0; kolom<3; kolom++){
					tictactoe[baris][kolom]=' ';
				}
			}
			do{
				int choise2 = -1;
				do{
					String pilihan="a";
					for(int baris=0; baris<3; baris++){
						for(int kolom=0; kolom<3; kolom++){
							tictactoe[baris][kolom]=' ';
						}
					}
					System.out.print("choise sign ( x / o ) : ");
					try{
						pilihan=scan.nextLine();
						if(pilihan.contentEquals("x") == false && pilihan.contentEquals("o") == false){
							throw new InputMismatchException();
						}
					}catch(InputMismatchException e){
						System.out.println("-----------------------------------");
						System.out.println("enter alphabet 'x' or 'o' ");
					}
					
						if(pilihan.contentEquals("x")){
							number=1;
							break;
						}
						if(pilihan.contentEquals("o")){
							number=0;
							break;
						}
						
				}while(true);
				
				while(true){
					int isi=0;
					System.out.println("===================================");
					System.out.println("\tGiliran Player Ke-"+pemain);
					System.out.println();
					System.out.println("\t  "+tictactoe[0][0]+" |   "+tictactoe[0][1]+"   | "+tictactoe[0][2]);
					System.out.println("\t____|_______|____");
					System.out.println("\t  "+tictactoe[1][0]+" |   "+tictactoe[1][1]+"   | "+tictactoe[1][2]);
					System.out.println("\t____|_______|____");
					System.out.println("\t  "+tictactoe[2][0]+" |   "+tictactoe[2][1]+"   | "+tictactoe[2][2]);
					System.out.println("\t    |       |    ");
					System.out.println("\n");
					
				
					if(		tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[0][1] && tictactoe[0][0]==tictactoe[0][2]||
							tictactoe[1][0]!=' ' && tictactoe[1][0]==tictactoe[1][1] && tictactoe[1][0]==tictactoe[1][2]||
							tictactoe[2][0]!=' ' && tictactoe[2][0]==tictactoe[2][1] && tictactoe[2][0]==tictactoe[2][2]||
							tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[1][1] && tictactoe[0][0]==tictactoe[2][2]||
							tictactoe[0][2]!=' ' && tictactoe[0][2]==tictactoe[1][1] && tictactoe[0][2]==tictactoe[2][0]||
							tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[1][0] && tictactoe[0][0]==tictactoe[2][0]||
							tictactoe[0][1]!=' ' && tictactoe[0][1]==tictactoe[1][1] && tictactoe[0][1]==tictactoe[2][1]||
							tictactoe[0][2]!=' ' && tictactoe[0][2]==tictactoe[1][2] && tictactoe[0][2]==tictactoe[2][2])
					{
						if(pemain==1)pemain++;
						else if(pemain==2)pemain--;
						System.out.println("GAME OVER, Player KE-"+pemain+" WIN");
						do{
							try{
								System.out.print("press 1 to play again dan 0 to out : ");
								choise2=scan.nextInt();
								
								if((choise2==1) == false && (choise2==0) == false){
									throw new InputMismatchException();
								}
							}
							catch(InputMismatchException e){
								System.out.println("------------------------------");
								System.out.println("enter number 1 or 0");
								scan.nextLine();
								continue;
							}break;
						}while(true);
						break;
					}
					
					
					for(int baris=0; baris<3; baris++){
						for(int kolom=0; kolom<3; kolom++){
							if(tictactoe[baris][kolom]!=' '){
								isi++;
							}
						}
					}if (isi==9){
						System.out.println("GAME OVER, KEDUA PEMAIN SERI");
						do{
							try{
								System.out.print("press 1 to play again dan 0 to out : ");
								choise2=scan.nextInt();
								
								if((choise2==1) == false && (choise2==0) == false){
									throw new InputMismatchException();
								}
							}
							catch(InputMismatchException e){
								System.out.println("------------------------------");
								System.out.println("enter number 1 or 0");
								scan.nextLine();
								continue;
							}break;
						}while(true);
						break;
					}
					
					
					System.out.println("CHOISE NUMBER ON THE MENU TO PLAY :");
					if(tictactoe[0][0]==' '){
						System.out.println("1. Baris 1 Kolom 1");
					}
					if(tictactoe[0][1]==' '){
						System.out.println("2. Baris 1 Kolom 2");
					}
					if(tictactoe[0][2]==' '){
						System.out.println("3. Baris 1 Kolom 3");
					}
					if(tictactoe[1][0]==' '){
						System.out.println("4. Baris 2 Kolom 1");
					}
					if(tictactoe[1][1]==' '){
						System.out.println("5. Baris 2 Kolom 2");
					}
					if(tictactoe[1][2]==' '){
						System.out.println("6. Baris 2 Kolom 3");
					}
					if(tictactoe[2][0]==' '){
						System.out.println("7. Baris 3 Kolom 1");
					}
					if(tictactoe[2][1]==' '){
						System.out.println("8. Baris 3 Kolom 2");
					}
					if(tictactoe[2][2]==' '){
						System.out.println("9. Baris 3 Kolom 3");
					}
					do{
						
						System.out.print("> ");
						pilih =-1;
						try{
							pilih =scan.nextInt();
							break;
						}catch(InputMismatchException e){
							System.out.println("Pilih menu yang Tersedia");
							scan.nextLine();
							pilih=-1;
							continue;
						}
					}while(true);
					
					switch(pilih){
					case 1:
						if(tictactoe[0][0]==' ' && pilih==1){
							if(number==1){
								tictactoe[0][0]='x';
								number--;
							}
							else if(number==0){
								tictactoe[0][0]='o';
								number++;
							}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
						}
						else if(tictactoe[0][0]!=' ' && pilih==1){
							System.out.println("Masukkan Pilihan di Ruang yang Kosong");
						}break;
					case 2:
						if(tictactoe[0][1]==' ' && pilih==2){
							if(number==1){
								tictactoe[0][1]='x';
								number--;
							}
							else if(number==0){
								tictactoe[0][1]='o';
								number++;
							}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
						}
						else if(tictactoe[0][1]!=' ' && pilih==2){
							System.out.println("Masukkan Pilihan di Ruang yang Kosong");
						}break;
					case 3:
						if(tictactoe[0][2]==' ' && pilih==3){
							if(number==1){
								tictactoe[0][2]='x';
								number--;
							}
							else if(number==0){
								tictactoe[0][2]='o';
								number++;
							}	
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
						}
						else if(tictactoe[0][2]!=' ' && pilih==3){
							System.out.println("Masukkan Pilihan di Ruang yang Kosong");
						}
					case 4:
						if(tictactoe[1][0]==' ' && pilih==4){
							if(number==1){
								tictactoe[1][0]='x';
								number--;
							}
							else if(number==0){
								tictactoe[1][0]='o';
								number++;
							}	
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
						}
						else if(tictactoe[1][0]!=' ' && pilih==4){
							System.out.println("Masukkan Pilihan di Ruang yang Kosong");
						}break;
					case 5:
						if(tictactoe[1][1]==' ' && pilih==5){
							if(number==1){
								tictactoe[1][1]='x';
								number--;
							}
							else if(number==0){
								tictactoe[1][1]='o';
								number++;
							}	
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
						}
						else if(tictactoe[1][1]!=' ' && pilih==5){
							System.out.println("Masukkan Pilihan di Ruang yang Kosong");
						}break;
					case 6:
						if(tictactoe[1][2]==' ' && pilih==6){
							if(number==1){
								tictactoe[1][2]='x';
								number--;
							}
							else if(number==0){
								tictactoe[1][2]='o';
								number++;
							}	
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
						}
						else if(tictactoe[1][2]!=' ' && pilih==6){
							System.out.println("Masukkan Pilihan di Ruang yang Kosong");
						}break;
					case 7:
						if(tictactoe[2][0]==' ' && pilih==7){
							if(number==1){
								tictactoe[2][0]='x';
								number--;
							}
							else if(number==0){
								tictactoe[2][0]='o';
								number++;
							}	
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
						}
						else if(tictactoe[2][0]!=' ' && pilih==7){
							System.out.println("Masukkan Pilihan di Ruang yang Kosong");
						}break;
					case 8:
						if(tictactoe[2][1]==' ' && pilih==8){
							if(number==1){
								tictactoe[2][1]='x';
								number--;
							}
							else if(number==0){
								tictactoe[2][1]='o';
								number++;
							}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
						}
						else if(tictactoe[2][1]!=' ' && pilih==8){
							System.out.println("Masukkan Pilihan di Ruang yang Kosong");
						}break;
					case 9:
						if(tictactoe[2][2]==' ' && pilih==9){
							if(number==1){
								tictactoe[2][2]='x';
								number--;
							}
							else if(number==0){
								tictactoe[2][2]='o';
								number++;
							}	
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
						}
						else if(tictactoe[2][2]!=' ' && pilih==9){
							System.out.println("Masukkan Pilihan di Ruang yang Kosong");
						}break;
					default:
						System.out.println("Masukkan Pilihan Yang Sesuai");
						break;
					}				
				}
				
				if(choise2 == 1 ){
					continue;
				}
				else if(choise2 == 0 ){
					break;
				}
				
			}while(true);
	}
	
	public static void main(String[] args) {
		do{
			int pilih=-1;
				
			System.out.println();
			System.out.println("===========TIC TAC TOE===========");
			System.out.println("1. Play");
			System.out.println("0. Exit");
			System.out.print("Masukkan Pilihan : ");
			try{
				pilih=scan.nextInt();
			}catch(InputMismatchException e){
				System.out.println("enter number  1 or 0");
			}
			switch(pilih){
			case 1:
				scan.nextLine();
				play();
				break;
			case 0:
				System.exit(0);
			default:
			 	System.out.println();
				System.out.println("enter number 1 or 0");
				break;
			}
		
		}while(true);
    }	
}

		
		
	








