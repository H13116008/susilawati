
	import java.io.IOException;
	import java.io.BufferedReader;
	import java.io.InputStreamReader;
		public class plat_and_geometrica {
		public static void showMenu() {
	        System.out.println("Menu Rumus bangun datar: ");
	        System.out.println("1. Persegi");
	        System.out.println("2. Persegi Panjang");
	        System.out.println("3. Segitiga Siku - Siku");
	        System.out.println("4. jajargenjang ");
	        System.out.println("5. Trapesium sama kaki");
	        System.out.println("6. Layang-layang ");
	        System.out.println("7. Belah ketupat");
	        System.out.println("8. lingkaran");
	      
	        System.out.println("Menu Rumus bangun ruang: ");
	        System.out.println("9. kubus");
	        System.out.println("10. balok ");
	        System.out.println("11. limas ");
	        System.out.println("12. prisma segitiga");
	        System.out.println("13. tabung");
	        System.out.println("14. kerucut");
	        System.out.println("15. bola");
	   }
		
		public static void squareFormula() {
	        System.out.println("enter input side (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputData = null;
	        try {
	            inputData = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float side = Float.parseFloat(inputData);
	            float large = side * side;
	           System.out.println("Luas Persegi: " + large);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }

	    public static void rectangleFormula() {
	        System.out.println("Masukan Panjang Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataLength = null;
	        try {
	            inputDataLength = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        System.out.println("Masukan Lebar Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataWide = null;
	        try {
	            inputDataWide = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float length = Float.parseFloat(inputDataLength);
	            float wide = Float.parseFloat(inputDataWide);
	            float large = length * wide;
	            System.out.println("Luas Persegi Panjang: " + large);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }

	    public static void triangleFormula() {
	    	System.out.println(" input length triangel (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataLength = null;
	        try {
	            inputDataLength = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("long input side of the triangel (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataWide = null;
	        try {
	            inputDataWide = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float length = Float.parseFloat(inputDataLength);
	            float wide = Float.parseFloat(inputDataWide);
	            double large = 0.5 * length * wide;
	            System.out.println("Luas segitiga: " + large);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	   
	    public static void parallelogram() {
	        System.out.println("input base  (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatabase = null;
	        try {
	            inputDatabase = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("input parallel sides (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataparallelsides = null;
	        try {
	            inputDataparallelsides = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float base = Float.parseFloat(inputDatabase);
	            float parallelsides = Float.parseFloat(inputDataparallelsides);
	            double large = base * parallelsides;
	            double roving = 2*(base + parallelsides);
	            System.out.println("Luas jajargenjang: " + large);
	            System.out.println("keliling jajargenjang : " + roving);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	   
	    public static void Trapezoidal() {
	        System.out.println("Masukan nilai panjang sisi  (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatalengthside = null;
	        try {
	            inputDatalengthside = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan nilai Sisi B  (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatasideB = null;
	        try {
	            inputDatasideB = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan nilai sisi sejajar dengan alas (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataparallel_side = null;
	        try {
	            inputDataparallel_side = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan nilai sisi D (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatasideD = null;
	        try {
	            inputDatasideD = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan tinggi  (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatahigh = null;
	        try {
	            inputDatahigh = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float lengthside= Float.parseFloat(inputDatalengthside);
	            float sideB = Float.parseFloat(inputDatasideB);
	            float parallel_side = Float.parseFloat(inputDataparallel_side);
	            float sideD = Float.parseFloat(inputDatasideD);
	            float high = Float.parseFloat(inputDatahigh);
	            double large = 0.5 * lengthside + parallel_side * high;
	            double roving = lengthside + sideB + parallel_side + sideD;
	            System.out.println("Luas trapesium: " + large);
	            System.out.println("keliling trapesium: " + roving);	            
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	    
	    public static void kiteformula() {
	        System.out.println("Masukan Panjang Sisi sejajar (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataLength = null;
	        try {
	            inputDataLength = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        System.out.println("Masukan Lebar Sisi sejajar (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataWide = null;
	        try {
	            inputDataWide = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan diagonal1 (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatadiagonal1 = null;
	        try {
	            inputDatadiagonal1 = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan diagonal2 (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatadiagonal2 = null;
	        try {
	            inputDatadiagonal2 = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float length = Float.parseFloat(inputDataLength);
	            float wide = Float.parseFloat(inputDataWide);
	            double diagonal1 = Double.parseDouble(inputDatadiagonal1);
	            double diagonal2 = Double.parseDouble(inputDatadiagonal2);
	            double large = 0.5 * diagonal1 * diagonal2;
	            float roving =2*(length + wide);
	            System.out.println("luas layang-layang: " + large);
	            System.out.println("keliling layang-layang: " + roving);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	    
	    public static void Diamondformula() {
	        System.out.println("Masukan panjang Sisi sejajar (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataSide = null;
	        try {
	            inputDataSide = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan diagonal1 (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatadiagonal1 = null;
	        try {
	            inputDatadiagonal1 = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan diagonal2 (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatadiagonal2 = null;
	        try {
	            inputDatadiagonal2 = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float Side = Float.parseFloat(inputDataSide);
	            float diagonal1 = Float.parseFloat(inputDatadiagonal1);
	            float diagonal2 = Float.parseFloat(inputDatadiagonal2);
	            double large = 0.5 * diagonal1 * diagonal2;
	            double roving = 4 * Side;
	            System.out.println("luas belah ketupat: " + large);
	            System.out.println("keliling belah ketupat: " + roving);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	    
	    public static void circleformula() {
	        System.out.println("Masukan panjang Sisi sejajar (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataSide = null;
	        try {
	            inputDataSide = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan diagonal1 (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatadiagonal1 = null;
	        try {
	            inputDatadiagonal1 = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan diagonal2 (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatadiagonal2 = null;
	        try {
	            inputDatadiagonal2 = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float Side = Float.parseFloat(inputDataSide);
	            float diagonal1 = Float.parseFloat(inputDatadiagonal1);
	            float diagonal2 = Float.parseFloat(inputDatadiagonal2);
	            double large = 0.5 * diagonal1 * diagonal2;
	            double roving = 4 * Side;
	            System.out.println("luas belah ketupat: " + large);
	            System.out.println("keliling belah ketupat: " + roving);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	   
	    public static void cubeformula() {
	        System.out.println("Masukan Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataside = null;
	        try {
	            inputDataside = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        try  {
	            float side = Float.parseFloat(inputDataside);
	            double large  = 6 * (Math.pow(side, 2));
	            double volume = (Math.pow(side, 3)) ;
	            System.out.println("luas kubus: " + large);
	            System.out.println("volume kubus: " + volume);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	   
	    public static void Beamformula() {
	        System.out.println("Masukan panjang balok (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatalength = null;
	        try {
	            inputDatalength = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan lebar balok (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatawide = null;
	        try {
	            inputDatawide = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan tinggi balok (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatahigh = null;
	        try {
	            inputDatahigh = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float length = Float.parseFloat(inputDatalength);
	            float wide = Float.parseFloat(inputDatawide);
	            float high = Float.parseFloat(inputDatahigh);
	            double large = 2 * (length*wide) + (length*high) + (wide*high);
	            double volume = length * wide * high;
	            System.out.println("luas permukaan balok : " + large);
	            System.out.println("volume balok: " + volume);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	    
	    public static void pyramidformula() {
	    	System.out.println("Enter luas alas limas (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataarea = null;
	        try {
	            inputDataarea = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Enter jumlah segitiga pada bidang tegak (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatatriangel_on_the_field = null;
	        try {
	            inputDatatriangel_on_the_field = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan tinggi  (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatahigh = null;
	        try {
	            inputDatahigh = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float area = Float.parseFloat(inputDataarea);
	            float triangel_on_the_field = Float.parseFloat(inputDatatriangel_on_the_field);
	            float high = Float.parseFloat(inputDatahigh);
	            double large = area + triangel_on_the_field;
	            double volume = 0.3 * area *high;
	            System.out.println("luas permukaan prisma : " + large);
	            System.out.println("volume prisma: " + volume);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	    
	    public static void prismformula() {
	        System.out.println("Masukan luas alas (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataarea = null;
	        try {
	            inputDataarea = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan keliling prisma (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataroving = null;
	        try {
	            inputDataroving = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan tinggi  (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatahigh = null;
	        try {
	            inputDatahigh = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float area = Float.parseFloat(inputDataarea);
	            float roving = Float.parseFloat(inputDataroving);
	            float high = Float.parseFloat(inputDatahigh);
	            double large = (2 * area) + (roving * high);
	            double volume = area * high ;
	            System.out.println("luas permukaan prisma : " + large);
	            System.out.println("volume prisma: " + volume);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	    
	    public static void Tubeformula() {
	        System.out.println("Masukan jari-jari tabung (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataRadius = null;
	        try {
	            inputDataRadius = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan tinggi  (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatahigh = null;
	        try {
	            inputDatahigh = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan phi (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataphi = null;
	        try {
	            inputDataphi = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float radius = Float.parseFloat(inputDataRadius);
	            float high = Float.parseFloat(inputDatahigh);
	            float phi = Float.parseFloat(inputDataphi);
	            double large = 2 * phi * (Math.pow(radius, 2)) * high;
	            double wide  = 2 * phi * radius * high;
	            double volume = phi * (Math.pow(radius, 2)) * high ;
	            System.out.println("luas permukaan tabung: " + large);
	            System.out.println("luas selimut tabung: " + wide);
	            System.out.println("volume tabung: " + volume);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	    
	    public static void Coneformula() {
	        System.out.println("Masukan phi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataphi = null;
	        try {
	            inputDataphi = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan jari-jari   (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataRadius = null;
	        try {
	            inputDataRadius = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan alas (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatabase= null;
	        try {
	            inputDatabase = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan tinggi (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatahigh= null;
	        try {
	            inputDatahigh = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float phi = Float.parseFloat(inputDataphi);
	            float radius = Float.parseFloat(inputDataRadius);
	            float base = Float.parseFloat(inputDatabase);
	            float high = Float.parseFloat(inputDatahigh);
	            double large = phi * (Math.pow(radius, 2)) * base;
	            double wide  = phi * radius * base;
	            double volume = 0.3 * (Math.pow(phi, 2)) * high;  
	            System.out.println("luas permukaan kerucut: " + large);
	            System.out.println("luas selimut kerucut: " + wide);
	            System.out.println("volume kerucut: " + volume);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	    
	    public static void Ballformula() {
	        System.out.println("Enter phi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataphi = null;
	        try {
	            inputDataphi = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Enter Radius   (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataRadius = null;
	        try {
	            inputDataRadius = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float phi = Float.parseFloat(inputDataphi);
	            float radius = Float.parseFloat(inputDataRadius);
	            double large = 4 * phi * (Math.pow(radius, 2));
	            double volume = 1.3 *phi * (Math.pow(radius, 3));  
	            System.out.println("luas permukaan kerucut: " + large);
	            System.out.println("volume kerucut: " + volume);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Anda Tidak Sesuai");
	        }
	    }
	    
	    public static void main(String[] args) {
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputData = null;
	        int choice = 0;
	        do {
	            showMenu();
	            System.out.println("Masukkan Pilihan Anda: ");
	            try {
	                inputData = bufferedReader.readLine();
	                try  {
	                    choice = Integer.parseInt(inputData);
	                    if (choice > 0 && choice == 1) {
	                        squareFormula();
	                    }
	                    else if (choice > 0 && choice == 2) {
	                        rectangleFormula();
	                    }
	                    else if (choice > 0 && choice == 3) {
	                    	triangleFormula();
	                    }
	                    else if (choice > 0 && choice == 4) {
		                    parallelogram();
	                    }
	                    else if (choice > 0 && choice == 5) {
	                    	Trapezoidal();
	                    }
	                    else if (choice > 0 && choice == 6) {
	                    	kiteformula();
	                    }
	                    else if (choice > 0 && choice == 7) {
	                    	Diamondformula();
	                    }
	                    else if (choice > 0 && choice == 8) {
	                    	circleformula();
	                    }
	                    else if (choice > 0 && choice == 9) {
	                    	cubeformula();
	                    }
	                    else if (choice > 0 && choice == 10) {
	                    	Beamformula();
	                    }
	                    else if (choice > 0 && choice == 11) {
	                    	pyramidformula();
	                    }
	                    else if (choice > 0 && choice == 12) {
	                    	prismformula();
	                    }
	                    else if (choice > 0 && choice == 13) {
	                    	Tubeformula();
	                    }
	                    else if (choice > 0 && choice == 14) {
	                    	Coneformula();
	                    }
	                    else if (choice > 0 && choice == 15) {
	                    	Ballformula();
	                    }
	                    else {
	                        System.out.println("Susilawati......");
	                    }
	                }
	                catch(NumberFormatException e) {
	                    System.out.println("Masukan Anda Tidak Sesuai");
	                }
	            }
	            catch (IOException error) {
	                System.out.println("Error Input " + error.getMessage());
	            }

	        } while(choice > 0);
	    }
	}



